﻿using Xunit;

namespace SeleniumPOC.Tests
{
    public class FactTest
    {
        [Fact]
        public void PassingTest()
        {
            Assert.Equal(8, Add(3, 2));
        }

        [Fact]
        public void FailingTest()
        {
            Assert.Equal(6, Add(2, 2));
        }

        int Add(int x, int y)
        {
            return x + x + y;
        }
    }
}
