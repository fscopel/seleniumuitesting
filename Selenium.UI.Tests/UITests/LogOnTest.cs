﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Xunit;

namespace Selenium.UI.Tests.UITests
{
    public class LogOnTest
    {
        IWebDriver _wd;

        public LogOnTest()
        {
            _wd = new ChromeDriver("WebDrivers");
        }

        [Fact]
        public void OpenECM()
        {
            _wd.Navigate().GoToUrl("http://dev-test3.northstarenterprise.com");
          
            Assert.True(_wd.Title == "Log On - ECM");
        }

        [Fact]
        public void LogOnToECM()
        {
            _wd.Navigate().GoToUrl("http://dev-test3.northstarenterprise.com");
            IWebElement query = _wd.FindElement(By.Id("UserName"));
            query.SendKeys("cbs");

            query = _wd.FindElement(By.Id("Password"));
            query.SendKeys("cbs7");

            query.Submit();

            Assert.True(_wd.Title == "Home - ECM");
        }

        ~LogOnTest()
        {
            Dispose();
        }

        public void Dispose()
        {
            _wd.Close();
            _wd.Dispose();
            GC.SuppressFinalize(this);
        }

    }
}
